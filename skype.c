/*
    Plugin for DeaDBeeF music player to set skype mood
    Copyright (C) 2014 Michael Ershov <mer13rain@gmail.com>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <deadbeef/deadbeef.h>
#include <dbus/dbus.h>
#include <assert.h>

#ifdef DEBUG
#define trace(...) { fprintf(stderr, __VA_ARGS__); }
#else
#define trace(...)
#endif

static DB_misc_t plugin;
static DB_functions_t *deadbeef;

#define MAX_CMD_LEN 512

DB_plugin_t*
ddb_skype_load(DB_functions_t *ddb)
{
    deadbeef = ddb;
    return DB_PLUGIN(&plugin);
}


#define SKYPE_NAME  "com.Skype.API"
#define SKYPE_OPATH "/com/Skype"
#define SKYPE_IFACE "com.Skype.API"
#define SKYPE_METHOD "Invoke"

static void
trace_error(const char* message, const DBusError* error)
{
	trace(message);
	trace(error->name);
	trace("\n");
	trace(error->message);
	trace("\n");
}

static DBusMessage*
create_message(const char* command)
{
	trace(command);
	DBusMessage* msg = NULL;

	msg = dbus_message_new_method_call(SKYPE_NAME, /* destination */
			SKYPE_OPATH,  /* obj. path */
			SKYPE_IFACE,  /* interface */
			SKYPE_METHOD); /* method str */
	if (msg == NULL)
	{
		trace("Ran out of memory when creating a message\n");
		return NULL;
	}

	dbus_message_set_no_reply(msg, TRUE);

	if (!dbus_message_append_args(msg,
				DBUS_TYPE_STRING, &command,
				DBUS_TYPE_INVALID))
	{
		trace("Ran out of memory while constructing args\n");
		return NULL;
	}

	return msg;
}

static int send_message(DBusConnection* bus, DBusMessage* msg)
{
	if (!dbus_connection_send(bus, msg, NULL))
	{
		trace("Ran out of memory while queueing message\n");
		return -1;
	}
	return 0;
}

static int
set_skype_mood(const char* artist, const char* title)
{
	DBusConnection* bus = NULL;
	DBusMessage* nameMsg = NULL;
	DBusMessage* protocolMsg = NULL;
	DBusMessage* moodMsg = NULL;

	DBusError error;

	const char* name = "NAME deadbeef";
	const char* protocol = "PROTOCOL 2";

	char moodCommand[MAX_CMD_LEN] = {0};
	if (artist)
	{
		snprintf(moodCommand, MAX_CMD_LEN, "SET PROFILE MOOD_TEXT %s - %s", artist, title);
	}
	else
	{
		snprintf(moodCommand, MAX_CMD_LEN, "SET PROFILE MOOD_TEXT");
	}

	dbus_error_init(&error);

	bus = dbus_bus_get(DBUS_BUS_SESSION, &error);
	if (dbus_error_is_set(&error))
	{
		trace_error("Failed to open Session bus\n", &error);
		return -1;
	}

	assert(bus != NULL);
	if (!dbus_bus_name_has_owner(bus, SKYPE_NAME, &error))
	{
		trace("Name has no owner on the bus!\n");
		dbus_connection_unref(bus);
		return -1;
	}
	if (dbus_error_is_set(&error))
	{
		trace_error("Failed to check for name ownership\n", &error);
		dbus_connection_unref(bus);
		return -1;
	}

	nameMsg = create_message(name);
	if (!nameMsg)
	{
		dbus_connection_unref(bus);
		return -1;
	}
	if (send_message(bus, nameMsg) == -1)
	{
		dbus_message_unref(nameMsg);
		dbus_connection_unref(bus);
		return -1;
	}

	protocolMsg = create_message(protocol);
	if (!protocolMsg)
	{
		dbus_message_unref(nameMsg);
		dbus_connection_unref(bus);
		return -1;
	}
	if (send_message(bus, protocolMsg) == -1)
	{
		dbus_message_unref(nameMsg);
		dbus_message_unref(protocolMsg);
		dbus_connection_unref(bus);
		return -1;
	}

	moodMsg = create_message(moodCommand);
	if (!moodMsg)
	{
		dbus_message_unref(nameMsg);
		dbus_message_unref(protocolMsg);
		dbus_connection_unref(bus);
		return -1;
	}
	if (send_message(bus, moodMsg) == -1)
	{
		dbus_message_unref(nameMsg);
		dbus_message_unref(protocolMsg);
		dbus_message_unref(moodMsg);
		dbus_connection_unref(bus);
		return -1;
	}

	dbus_connection_flush(bus);

	dbus_message_unref(nameMsg);
	dbus_message_unref(protocolMsg);
	dbus_message_unref(moodMsg);
	nameMsg = NULL;
	protocolMsg = NULL;
	moodMsg = NULL;

	dbus_connection_unref(bus);
	bus = NULL;

	return 0;
}

static int
get_artist_info(DB_playItem_t* track, char** artist)
{
	deadbeef->pl_lock();

    const char *cur_artist = deadbeef->pl_find_meta(track, "artist");
    if (!cur_artist)
	{
        deadbeef->pl_unlock();
        return -1;
    }
    size_t alen = strlen(cur_artist);

    *artist = calloc(alen + 1, sizeof(char));
    if (!*artist)
	{
        deadbeef->pl_unlock();
        return -1;
    }
    memcpy(*artist, cur_artist, alen + 1);
	deadbeef->pl_unlock();
	return 0;
}

static int
get_title_info(DB_playItem_t* track, char** title)
{
    deadbeef->pl_lock();

    const char *cur_title = deadbeef->pl_find_meta(track, "title");
    if (!cur_title)
	{
        deadbeef->pl_unlock();
        return -1;
    }
    size_t tlen = strlen(cur_title);

    *title = calloc(tlen + 1, sizeof(char));
    if (!*title)
	{
        deadbeef->pl_unlock();
        return -1;
    }
    memcpy(*title, cur_title, tlen + 1);
    deadbeef->pl_unlock();
    return 0;
}

static int 
get_artist_and_title(DB_playItem_t* track, char** artist, char** title)
{
	if (!track)
	{
		return 0;
	}
	if (get_artist_info(track, artist) == -1)
	{
		return -1;
	}
	if (get_title_info(track, title) == -1)
	{
		free(*artist);
		return -1;
	}
	return 0;
}

static int
song_changed(ddb_event_trackchange_t* ev)
{
	char* artist = NULL;
	char* title = NULL;

	if (get_artist_and_title(ev->to, &artist, &title) == -1)
	{
		return -1;
	}

	set_skype_mood(artist, title);

	if (artist)
	{
		free(artist);
	}
	if (title)
	{
		free(title);
	}

	return 0;
}

static int
skype_message(uint32_t id, uintptr_t ctx, uint32_t p1, uint32_t p2)
{
	switch (id)
	{
		case DB_EV_SONGCHANGED:
			return song_changed((ddb_event_trackchange_t*)ctx);
	}
	return 0;
}

// define plugin interface
static DB_misc_t plugin =
{
    DB_PLUGIN_SET_API_VERSION
    .plugin.version_major = 0,
    .plugin.version_minor = 1,
    .plugin.type = DB_PLUGIN_MISC,
    .plugin.name = "Skype",
    .plugin.descr = "Plugin for DeaDBeeF music player to set skype mood",
    .plugin.copyright =
        "Copyright (C) 2014 Michael Ershov <mer13rain@gmail.com>\n"
        "\n"
        "This program is free software; you can redistribute it and/or\n"
        "modify it under the terms of the GNU General Public License\n"
        "as published by the Free Software Foundation; either version 2\n"
        "of the License, or (at your option) any later version.\n"
        "\n"
        "This program is distributed in the hope that it will be useful,\n"
        "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
        "GNU General Public License for more details.\n"
        "\n"
        "You should have received a copy of the GNU General Public License\n"
        "along with this program; if not, write to the Free Software\n"
        "Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n"
    ,
    .plugin.website = "https://bitbucket.org/13rain/deadbeef-skype",
	.plugin.message = skype_message
};

