PREFIX?=/usr

OUT=ddb_skype.so
INCLUDE=-I${PREFIX}/include -I${PREFIX}/include/dbus-1.0
CC?=gcc
CFLAGS+=-Wall -fPIC -D_GNU_SOURCE ${INCLUDE}
LDFLAGS+=-shared -ldbus-1

SOURCES=skype.c

OBJECTS=$(SOURCES:.c=.o)

all: $(SOURCES) $(OUT)

$(OUT): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -c -o $@

clean:
	rm $(OBJECTS) $(OUT)
